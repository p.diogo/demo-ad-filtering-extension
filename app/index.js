document.addEventListener("DOMContentLoaded", () => {
    const filterInput = document.getElementById("addFilterInput");
    document.getElementById("addFilterButton").addEventListener("click", () => {
        chrome.runtime.sendMessage({
            msg: "addFilter",
            filterText: filterInput.value
        });
        filterInput.value = "";
    });
    chrome.runtime.onMessage.addListener(
        function (request) {
            if (request.msg == "updateCounter") {
                document.getElementById("totalBlocks").innerText = request.totalBlocks;
                document.getElementById("tabBlocks").innerText = request.blocksThisPage;
            }
        }
    );
    chrome.runtime.sendMessage({
        msg: "requestCounters"
    });
});