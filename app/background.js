let totalBlocks = 0;
const blocksByTab = new Map();

function addFilter(filterText) {
    EWE.filters.add([filterText]);
}

function getCurrentTab(callback) {
    let queryOptions = { active: true, lastFocusedWindow: true };
    chrome.tabs.query(queryOptions, ([tab]) => {
        if (chrome.runtime.lastError)
            console.error(chrome.runtime.lastError);
        // `tab` will either be a `tabs.Tab` instance or `undefined`.
        callback(tab);
    });
}

function setUpMessageListener() {
    chrome.runtime.onMessage.addListener(
        function (request) {
            if (request.msg === "addFilter")
                addFilter(request.filterText);
            if (request.msg === "requestCounters")
                getCurrentTab((tab) => { updateCounterView(tab?.id) });
        }
    );
}

function updateCounterView(tabId) {
    chrome.runtime.sendMessage({
        msg: "updateCounter",
        totalBlocks,
        blocksThisPage: blocksByTab.get(tabId) || 0
    })
}

function setUpReporting() {
    EWE.reporting.start();
    EWE.reporting.onBlockableItem.addListener(event => {
        totalBlocks += 1;
        const tabId = event.request.tabId;
        blocksByTab.set(tabId, (blocksByTab.get(tabId) || 0) + 1);
        updateCounterView(tabId);
    });
    chrome.tabs.onUpdated.addListener(function (tabId, changeInfo) {
        if (changeInfo.status == 'loading') {
            blocksByTab.set(tabId, 0);
            updateCounterView(tabId);
        }
    });
}

async function init() {
    const defaultSubsList = (await fetch('app/subscriptions.json')).json();
    await EWE.start({ bundledSubscriptions: defaultSubsList });
    await EWE.subscriptions.addDefaults();
    setUpMessageListener();
    setUpReporting();
}

init();