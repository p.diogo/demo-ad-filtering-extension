# Demo Ad-Filtering Extension

## Description
This is a Chrome extension that filter ads. It was build to showcase the functionalities of [Eyeo's WebExt SDK](https://gitlab.com/eyeo/adblockplus/abc/webext-sdk). This SDK makes the process of developing ad-filtering software very easy and smooth.

This extension does:
- filter ads
- accept custom filters defined by the user
- count how many ads were blocked in total and by page

## Installation
To load this extension into Google Chrome go to menu > more tools > Extensions or navigate to chrome://extensions/ and make sure that the developer mode is activated. Click on Load Unpacked and select the root folder of the project.

## Usage
As soon as you load the extension into Chrome it is already blocking ads.
To add custom filters just click on the extension icon to open the extension pop-up, fill in your filter in the input field and click the "add filter" button.